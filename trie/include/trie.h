#ifndef INCLUDE_TRIE_H_
#define INCLUDE_TRIE_H_

#include <algorithm>
#include <numeric>
#include <string>
#include <utility>
#include <vector>

class Trie {
 public:
    Trie();
    void add(const std::string& str);
    size_t countNodes();
    bool contains(const std::string& str) const;

 private:
    struct Node {
        Node& operator=(const Node& rhs) = delete;

        char value;
        std::vector<std::pair<char, Node>> children;

        bool isLeaf() const {
            return children.empty();
        }

        bool containsChild(char c) const {
            return std::find_if(children.cbegin(), children.cend(),
                                [c](const std::pair<char, Node>& kv) {
                                    return c == kv.first;
                                }) != children.cend();
        }

        size_t countSubnodes() const {
            return children.size() +
                   std::accumulate(
                       children.cbegin(), children.cend(), 0,
                       [](size_t c, const std::pair<char, Node>& kv) {
                           return c + kv.second.countSubnodes();
                       });
        }
    };

    Node root;
};

#endif  // INCLUDE_TRIE_H_
