#include <algorithm>
#include <functional>
#include <string>

#include "../include/trie.h"

Trie::Trie() {
}

void Trie::add(const std::string& str) {
    Node* i = &root;
    for (const char c : str) {
        if (!i->containsChild(c)) {
            i->children.push_back(std::make_pair(c, Node()));
        }
        i = &std::find_if(
                 i->children.begin(), i->children.end(),
                 [c](const std::pair<char, Node>& kv) { return kv.first == c; })
                 ->second;
    }
}

bool Trie::contains(const std::string& str) const {
    const Node* i = &root;
    for (const char c : str) {
        if (!i->containsChild(c)) {
            return false;
        }
        i = &std::find_if(
                 i->children.begin(), i->children.end(),
                 [c](const std::pair<char, Node>& kv) { return kv.first == c; })
                 ->second;
    }
    return i->isLeaf();
}

size_t Trie::countNodes() {
    return root.countSubnodes();
}
