#include <array>
#include <cstdio>
#include <functional>
#include <memory>

#include "../include/trie.h"

void test() {
    Trie t;
    t.add("abcxyz");
    t.add("abcabc");
    t.add("abcabd");
    std::printf("%zu\n", t.countNodes());
    std::printf("abcxyz is %s trie\n", t.contains("abcxyz") ? "in" : "not in");
    std::printf("abc is %s trie\n", t.contains("abc") ? "in" : "not in");
    std::printf("abcxyzabc is %s trie\n",
                t.contains("abcxyzabc") ? "in" : "not in");
    std::printf("abcabc is %s trie\n", t.contains("abcabc") ? "in" : "not in");
}

int main(int argc, char** argv) {
    if (argc < 2) {
        std::printf("Wrong argc\n");
        return 1;
    }
    Trie t;
    std::unique_ptr<FILE, std::function<void(FILE*)>> fp(
        std::fopen(argv[1], "rb"), [](std::FILE* ptr) { std::fclose(ptr); });
    std::array<char, 256> buf;
    while (fscanf(fp.get(), "%s", buf.data()) != EOF) {
        t.add(buf.data());
    }
    std::printf("%zu\n", t.countNodes());
    return 0;
}
