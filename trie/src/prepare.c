#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main(int argc, char** argv) {
    srand((unsigned int)time(NULL));
    if (argc < 2) {
        printf("Wrong argc\n");
        return 0;
    }
    FILE* fp = fopen(argv[1], "rb");
    unsigned char buf[256];
    char print_buf[256];
    int count = rand() % 12 + 1;
    int bytes_read = 0;
    while ((bytes_read = fread(buf, sizeof(unsigned char), count, fp)) != 0) {
        int printed_chars = 0;
        for (int i = 0; i < bytes_read; i++) {
            int tmp =
                snprintf(print_buf + printed_chars,
                         sizeof(print_buf) - printed_chars, "%u", *(buf + i));
            printed_chars += tmp;
        }
        print_buf[printed_chars] = '\0';
        printf("%s\n", print_buf);
        count = rand() % 12 + 1;
    }
    fclose(fp);
    return 0;
}
